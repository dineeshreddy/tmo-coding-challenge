import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { FetchPriceQuery } from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { getSelectedSymbol, getAllPriceQueries } from './price-query.selectors';
import { map, skip } from 'rxjs/operators';

@Injectable()
export class PriceQueryFacade {
public startDate: Date ;
public endDate: Date;

  selectedSymbol$ = this.store.pipe(select(getSelectedSymbol));
  priceQueries$ = this.store.pipe(
    
    select(getAllPriceQueries),
    skip(1),
    map(priceQueries => priceQueries.filter((x) => {
      return this.startDate ? (new Date(x.date).getTime() >= this.startDate.getTime() &&
      new Date(x.date).getTime() <= this.endDate.getTime()) : priceQueries
     }
    )
  ), map(priceQueries=> priceQueries.map(priceQuery => [priceQuery.date,priceQuery.close])));

  constructor(private store: Store<PriceQueryPartialState>) {}

  fetchQuote(symbol: string, period: string, startDate?:Date, endDate?:Date) {
    this.startDate = startDate;
    this.endDate = endDate;
    this.store.dispatch(new FetchPriceQuery(symbol, period));

  }

}
